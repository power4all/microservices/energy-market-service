﻿using EnergyMarketService.Models;
using EnergyMarketService.Models.Requests;
using EnergyMarketService.Models.Responses;
using Flurl.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnergyMarketService.Logic
{
    public class SellService
    {
        private readonly CentralApiClient Client = new CentralApiClient();

        //POST /api/offer
        public async Task<int> AddOffer(BuyOfferRequest request)
        {
            var response = await Client.Request("/offer")
                .PostJsonAsync(request);

            return response.StatusCode;
        }

        //DELETE /api/offer
        public async Task<IActionResult> DeleteOffer(string offerId)
        {
            var response = await Client.Request("/offer")
                .SetQueryParam("offerId", offerId)
                .GetJsonAsync();

            return response;
        }

        //GET /api/offer
        public async Task<OfferResponse> GetOffer(string offerId)
        {
            var response = await Client.Request("/offer")
                .SetQueryParam("offerId", offerId)
                .GetJsonAsync<OfferResponse>();

            return response;
        }

        //GET /api/offer/all
        public async Task<List<OfferResponse>> AllOffers()
        {
            var response = await Client.Request("/offer/all")
                .GetAsync()
                .ReceiveJson<List<OfferResponse>>();

            return response;
        }

        //GET /api/offer/own
        public async Task<List<OfferResponse>> OwnOffers()
        {
            var response = await Client.Request("/offer/own")
                .GetAsync()
                .ReceiveJson<List<OfferResponse>>();

            return response;
        }
    }
}
