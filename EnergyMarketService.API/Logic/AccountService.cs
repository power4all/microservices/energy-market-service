﻿using EnergyMarketService.Models;
using EnergyMarketService.Models.Requests;
using Flurl.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnergyMarketService.Logic
{
    public class AccountService
    {
        private readonly CentralApiClient Client = new CentralApiClient();

        //POST /api/account
        public async Task<IActionResult> Account(CreateAccountRequest request)
        {
            var response = await Client.Request("/account")
                .PostJsonAsync(request)
                .ReceiveJson();

            return response;
        }

        //POST /api/account/admin
        public async Task<IActionResult> Admin(CreateAccountRequest request)
        {
            var response = await Client.Request("/account/admin")
                .PostJsonAsync(request)
                .ReceiveJson();

            return response;
        }

        //POST /api/login
        public async Task<string> Login(LoginAccountRequest request)
        {
            var response = await Client.Request("/login")
              .PostJsonAsync(request)
              .ReceiveJson();

            return response;
        }

        //GET /api/balance
        public async Task<double> Balance()
        {
            var response = await Client.Request("/balance")
                .GetAsync()
                .ReceiveJson();

            return response;
        }
    }
}
