﻿using EnergyMarketService.Models;
using EnergyMarketService.Models.Requests;
using EnergyMarketService.Models.Responses;
using Flurl;
using Flurl.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EnergyMarketService.Logic
{
    public class BuyService
    {
        private readonly CentralApiClient Client = new CentralApiClient();

        //POST /api/buy
        public async Task<BuyOfferResponse> Buy(BuyOfferRequest request)
        {
            var response = await Client.Request("/buy")
                .PostJsonAsync(request)
                .ReceiveJson<BuyOfferResponse>();

            return response;
        }

        //GET /api/buy
        public async Task<List<BuyOfferResponse>> Buy()
        {
            var response = await Client.Request("/buy")
                .GetAsync()
                .ReceiveJson<List<BuyOfferResponse>>();

            return response;
        }

        //GET /api/buy/sales
        public async Task<List<BuyOfferResponse>> Sales()
        {
            var response = await Client.Request("/buy/sales")
                .GetAsync()
                .ReceiveJson<List<BuyOfferResponse>>();

            return response;
        }
    }
}
