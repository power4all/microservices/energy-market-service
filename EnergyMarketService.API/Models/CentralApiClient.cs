﻿using Flurl.Http;

namespace EnergyMarketService.Models
{
    public class CentralApiClient : FlurlClient
    {
        public CentralApiClient(
         
            string baseUrl = 
            //"http://localhost:8085/api",
            "http://shielded-bastion-42632.herokuapp.com/api",
            string jwtToken = "eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImNiMjQ1ZjJhLTAxZmUtNGVjOC1hZDNkLTc5N2ZjNzk3MzY5YyJ9.IBeG04dVu6s3HM7Xqpurwff4RFIf_qUpz7K9L4YlXE_HyezBVoJFA8uonfhhgV8sr1RlK402iaB-qFB5tXZJEw"
        )
            : base(baseUrl)
        {
            this.WithOAuthBearerToken(jwtToken);
        }
    }
}
