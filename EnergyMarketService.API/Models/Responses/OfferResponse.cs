﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnergyMarketService.Models.Responses
{
    public class OfferResponse
    {
        public string Id { get; set; }
        public string Seller { get; set; }
        public double Amount { get; set; }

        public double AmountTotal { get; set; }

        public double Price { get; set; }
    }
}
