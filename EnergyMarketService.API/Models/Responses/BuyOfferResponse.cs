﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace EnergyMarketService.Models.Responses
{
    public class BuyOfferResponse
    {
        [AllowNull]
        public string BuyerName { get; set; }

        public OfferResponse Offer { get; set; }
        public double Amount { get; set; }
        public double PriceTotal { get; set; }
    }
}
