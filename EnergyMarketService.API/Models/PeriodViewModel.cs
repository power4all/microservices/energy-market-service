﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EnergyMarketService.API.Models
{
    /// <summary>
    /// Model to verify incoming period request
    /// </summary>
    public class PeriodViewModel : IValidatableObject
    {
        [Required]
        [Display(Name = "StartDate")]
        [DataType(DataType.DateTime)]
        public DateTime StartDate { get; set; }

        [Required]
        [Display(Name = "EndDate")]
        [DataType(DataType.DateTime)]
        public DateTime EndDate { get; set; }

        //see https://stackoverflow.com/a/19882534     
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (EndDate < StartDate || StartDate > EndDate)
            {
                yield return new ValidationResult(
                    errorMessage: "start < end or end > start is not true",
                    memberNames: new[] { "StartDate", "EndDate" }
               );
            }
        }
    }
}
