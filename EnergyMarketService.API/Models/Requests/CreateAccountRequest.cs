﻿using System.ComponentModel.DataAnnotations;
using System.Configuration;

namespace EnergyMarketService.Models.Requests
{
    public class CreateAccountRequest
    {
        [Required, StringValidator(MinLength = 6)]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}
