﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EnergyMarketService.Models.Requests
{
    public class CreateSellOfferRequest
    {
        [Required, Range(1, double.MaxValue, ErrorMessage = "Please enter a value bigger than {0}")]
        public double Amount { get; set; }

        [Required, Range(0.01, double.MaxValue, ErrorMessage = "Please enter a value bigger than {0}")]
        public double Price { get; set; }
    }
}
