﻿using EnergyMarketService.Logic;
using EnergyMarketService.Models.Requests;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace EnergyMarketService.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly AccountService _accountService;

        public AccountController(AccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpPost]
        public async Task<IActionResult> Account(CreateAccountRequest request)
        {
            try
            {
                return await _accountService.Account(request);
            }
            catch (Exception e)
            {
                return Conflict(e.Message);
            }
        }

        [HttpPost("admin")]
        public async Task<IActionResult> Admin(CreateAccountRequest request)
        {
            try
            {
                return await _accountService.Admin(request);
            }
            catch (Exception e)
            {
                return Conflict(e.Message);
            }
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginAccountRequest request)
        {
            try
            {
                string login = await _accountService.Login(request);
                return Ok(login);
            }
            catch (Exception e)
            {
                return Conflict(e.Message);
            }
        }

        [HttpGet("balance")]
        public async Task<IActionResult> Balance()
        {
            try
            {
                double balance = await _accountService.Balance();
                return Ok(balance);
            }
            catch (Exception e)
            {
                return Conflict(e.Message);
            }
        }
    }
}
