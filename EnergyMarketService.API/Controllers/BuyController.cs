﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using EnergyMarketService.Models.Requests;
using EnergyMarketService.Models.Responses;
using System.Collections.Generic;
using EnergyMarketService.Models;
using EnergyMarketService.Logic;

namespace EnergyMarketService.API.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class BuyController : ControllerBase
	{
		private readonly BuyService _buyService;

		public BuyController(BuyService buyService)
        {
			_buyService = buyService;
        }

		[HttpPost]
		public async Task<IActionResult> Buy([FromBody] BuyOfferRequest request)
		{
			try
			{
				BuyOfferResponse response = await _buyService.Buy(request);
				return Ok(response);
			}
			catch(Exception e)
            {
				return Conflict(e.Message);
            }
		}

		[HttpGet]
		public async Task<IActionResult> Buy()
		{ 
			try
			{
				List<BuyOfferResponse> offers = await _buyService.Buy();
				return Ok(offers);
			}
			catch(Exception e)
            {
				return Conflict(e.Message);
            }

		}

		[HttpGet("sales")]
		public async Task<IActionResult> Sales()
		{
			try
			{
				List<BuyOfferResponse> offers = await _buyService.Sales();
				return Ok(offers);
			}
			catch(Exception e)
            {
				return Conflict(e.Message);
            }
		}
	}
}
