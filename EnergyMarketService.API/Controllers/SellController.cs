﻿using EnergyMarketService.Logic;
using EnergyMarketService.Models.Requests;
using EnergyMarketService.Models.Responses;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnergyMarketService.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SellController : ControllerBase
    {
		private readonly SellService _sellService;

		public SellController(SellService sellService)
		{
			_sellService = sellService;
		}

		[HttpPost("offer")]
		public async Task<IActionResult> AddOffer([FromBody] BuyOfferRequest request)
		{
			try
			{
				int statuscode = await _sellService.AddOffer(request);
				if (statuscode == 201)
				{
					return Ok();
				}
				return Conflict("unable to add offer");
			}
			catch (Exception e)
			{
				return Conflict(e.Message);
			}
		}

		[HttpDelete("offer")]
		public async Task<IActionResult> DeleteOffer([FromQuery] string offerId)
		{
			try
			{
				return await _sellService.DeleteOffer(offerId);
			}
			catch (Exception e)
			{
				return Conflict(e.Message);
			}
		}

		[HttpGet("offer")]
		public async Task<IActionResult> GetOffer([FromQuery] string offerId)
		{
			try
			{
				OfferResponse response = await _sellService.GetOffer(offerId);
				return Ok(response);
			}
			catch (Exception e)
			{
				return Conflict(e.Message);
			}
		}

		[HttpGet("offer/all")]
		public async Task<IActionResult> AllOffers()
		{
			try
			{
				List<OfferResponse> offers = await _sellService.AllOffers();
				return Ok(offers);
			}
			catch (Exception e)
			{
				return Conflict(e.Message);
			}
		}

		[HttpGet("offer/own")]
		public async Task<IActionResult> OwnOffers()
		{
			try
			{
				List<OfferResponse> offers = await _sellService.OwnOffers();
				return Ok(offers);
			}
			catch (Exception e)
			{
				return Conflict(e.Message);
			}
		}
	}
}
