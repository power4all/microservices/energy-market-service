using System;
using System.Net.Http;
using EnergyMarketService.Logic;
using EnergyMarketService.Data;
using EnergyMarketService.Helpers;
using EnergyMarketService.Logic.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace EnergyMarketService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<EnergyMarketDbContext>(options =>
            {
                options.UseMySql(
                    Configuration.GetConnectionString("MySql"),
                    new MySqlServerVersion(new Version(8, 0, 23)),
                mySqlOptions =>
                {
                    mySqlOptions.EnableRetryOnFailure(
                        maxRetryCount: 2);
                });
            });

            services.Configure<RouteOptions>(o => o.LowercaseUrls = true);
            services.AddControllers(o =>
            {
                o.Conventions.Add(new RouteTokenTransformerConvention(new SlugifyParameterTransformer()));
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "EnergyMarketService.API", Version = "v1" });
            });

            services.AddTransient<AccountService>();
            services.AddTransient<BuyService>();
            services.AddTransient<SellService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "EnergyMarketService.API v1"));
            }

            app.UseCors(options =>
            {
                options.WithOrigins("http://localhost:8080")
                .AllowAnyMethod().AllowAnyHeader();
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
