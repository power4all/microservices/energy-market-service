﻿using System;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace EnergyMarketService.Data
{
	public class EnergyMarketDbContext : DbContext
	{
		public EnergyMarketDbContext(DbContextOptions<EnergyMarketDbContext> options)
			: base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			base.OnConfiguring(optionsBuilder);
		}
	}
}